package com.example.airquality

import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.android.volley.Request
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.airquality.ui.theme.AirQualityTheme
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import org.json.JSONObject

class MainActivity : ComponentActivity() {
    private val locationPermission = android.Manifest.permission.ACCESS_COARSE_LOCATION
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        if (ContextCompat.checkSelfPermission(
                this,
                locationPermission
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(locationPermission),
                124
            )
        }

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.let {
                    setContent {
                        AirQualityTheme {
                            Surface(
                                modifier = Modifier.fillMaxSize(),
                                color = MaterialTheme.colorScheme.background
                            ) {
                                AirQualityScreen(location)
                            }
                        }
                    }
                }
            }
    }
}

@Composable
fun AirQualityScreen(location: Location?) {

    var locationText by remember { mutableStateOf("Loading...") }

    var airQualityValue : Int
    var airQualityComponents : JSONObject
    var airQuality by remember { mutableStateOf("Loading...") }
    var componentsCO by remember { mutableStateOf(0.0) }
    var componentsNO2 by remember { mutableStateOf(0.0) }
    var componentsO3 by remember { mutableStateOf(0.0) }
    var componentsSO2 by remember { mutableStateOf(0.0) }
    var componentsPM2d5 by remember { mutableStateOf(0.0) }
    var componentsPM10 by remember { mutableStateOf(0.0) }

    val requestQueue = Volley.newRequestQueue(LocalContext.current)

    fun checkAirQuality() {

        if (location == null) {
            locationText = "Error checking location"
            return
        }

        val apiKey = "af2a17dad0da92e678d49c632f8f7fc9"
        val apiAirQualityUrl = "https://api.openweathermap.org/data/2.5/air_pollution?" +
                "lat=${location.latitude}&lon=${location.longitude}&appid=$apiKey"
        val apiLocationUrl = "https://api.openweathermap.org/geo/1.0/reverse?" +
                "lat=${location.latitude}&lon=${location.longitude}&limit=1&appid=$apiKey"

        val locationRequest = JsonArrayRequest(
            Request.Method.GET, apiLocationUrl, null,
            { response ->
                locationText = response.getJSONObject(0).getString("name")
            },
            { error ->
                Log.e("AirQualityScreen", "Error fetching data: ${error.message}", error)
                airQuality = "Error fetching data"
            }
        )

        val airQualityRequest = JsonObjectRequest(
            Request.Method.GET, apiAirQualityUrl, null,
            { response ->

                airQualityValue = response.getJSONArray("list").getJSONObject(0)
                    .getJSONObject("main").getInt("aqi")
                airQualityComponents = response.getJSONArray("list").getJSONObject(0)
                    .getJSONObject("components")

                airQuality = AirQualityLevel.fromValue(airQualityValue).description
                componentsCO = airQualityComponents.getDouble("co")
                componentsNO2 = airQualityComponents.getDouble("no2")
                componentsO3 = airQualityComponents.getDouble("o3")
                componentsSO2 = airQualityComponents.getDouble("so2")
                componentsPM2d5 = airQualityComponents.getDouble("pm2_5")
                componentsPM10 = airQualityComponents.getDouble("pm10")
            },
            { error ->
                Log.e("AirQualityScreen", "Error fetching data: ${error.message}", error)
                airQuality = "Error fetching data"
            }
        )

        requestQueue.add(locationRequest)
        requestQueue.add(airQualityRequest)
    }

    Column(
        modifier = Modifier.fillMaxSize(0.8f),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(text = "Location: $locationText", style = MaterialTheme.typography.headlineSmall)

        Spacer(modifier = Modifier.height(64.dp))

        Text(text = "Air Quality: $airQuality", style = MaterialTheme.typography.headlineSmall)

        Spacer(modifier = Modifier.height(64.dp))

        Text(text = "Details:")

        Spacer(modifier = Modifier.height(16.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth(0.8f)
                .padding(horizontal = 4.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(text = "CO:", modifier = Modifier.weight(1f))
            Text(text = componentsCO.toString(), modifier = Modifier.weight(1f))
            Text(text = COLevel.fromValue(componentsCO).description, modifier = Modifier.weight(1f))
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(0.8f)
                .padding(horizontal = 4.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(text = "NO₂:", modifier = Modifier.weight(1f))
            Text(text = componentsNO2.toString(), modifier = Modifier.weight(1f))
            Text(text = NO2Level.fromValue(componentsNO2).description, modifier = Modifier.weight(1f))
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(0.8f)
                .padding(horizontal = 4.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(text = "O₃:", modifier = Modifier.weight(1f))
            Text(text = componentsO3.toString(), modifier = Modifier.weight(1f))
            Text(text = O3Level.fromValue(componentsO3).description, modifier = Modifier.weight(1f))
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(0.8f)
                .padding(horizontal = 4.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(text = "SO₂:", modifier = Modifier.weight(1f))
            Text(text = componentsSO2.toString(), modifier = Modifier.weight(1f))
            Text(text = SO2Level.fromValue(componentsSO2).description, modifier = Modifier.weight(1f))
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(0.8f)
                .padding(horizontal = 4.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(text = "PM₂.₅:", modifier = Modifier.weight(1f))
            Text(text = componentsPM2d5.toString(), modifier = Modifier.weight(1f))
            Text(text = PM2d5Level.fromValue(componentsPM2d5).description, modifier = Modifier.weight(1f))
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(0.8f)
                .padding(horizontal = 4.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(text = "PM₁₀:", modifier = Modifier.weight(1f))
            Text(text = componentsPM10.toString(), modifier = Modifier.weight(1f))
            Text(text = PM10Level.fromValue(componentsPM10).description, modifier = Modifier.weight(1f))
        }

        Spacer(modifier = Modifier.height(64.dp))

        Button(
            onClick = {
                locationText = "Rechecking..."
                airQuality = "Rechecking..."
                checkAirQuality()
            },
            modifier = Modifier
                .fillMaxWidth(0.8f)
                .height(48.dp)
        ) {
            Icon(imageVector = Icons.Default.Refresh, contentDescription = null)
            Spacer(modifier = Modifier.width(8.dp))
            Text("Recheck Air Quality")
        }

        LaunchedEffect(Unit) {
            checkAirQuality()
        }
    }
}

enum class AirQualityLevel(val value: Int, val description: String) {
    GOOD(1, "Good"),
    FAIR(2, "Fair"),
    MODERATE(3, "Moderate"),
    POOR(4, "Poor"),
    VERY_POOR(5, "Very poor"),
    UNKNOWN(0, "Unknown");

    companion object {
        fun fromValue(value: Int): AirQualityLevel {
            return values().firstOrNull { it.value == value } ?: UNKNOWN
        }
    }
}

enum class SO2Level(val value: Double, val description: String) {
    VERY_POOR(350.0, "Very poor"),
    POOR(250.0, "Poor"),
    MODERATE(80.0, "Moderate"),
    FAIR(20.0, "Fair"),
    GOOD(0.0, "Good"),
    UNKNOWN(0.0, "Unknown");

    companion object {
        fun fromValue(value: Double): SO2Level {
            return values().firstOrNull { it.value <= value } ?: UNKNOWN
        }
    }
}

enum class NO2Level(val value: Double, val description: String) {
    VERY_POOR(200.0, "Very poor"),
    POOR(150.0, "Poor"),
    MODERATE(70.0, "Moderate"),
    FAIR(40.0, "Fair"),
    GOOD(0.0, "Good"),
    UNKNOWN(0.0, "Unknown");

    companion object {
        fun fromValue(value: Double): NO2Level {
            return values().firstOrNull { it.value <= value } ?: UNKNOWN
        }
    }
}

enum class PM10Level(val value: Double, val description: String) {
    VERY_POOR(200.0, "Very poor"),
    POOR(100.0, "Poor"),
    MODERATE(50.0, "Moderate"),
    FAIR(20.0, "Fair"),
    GOOD(0.0, "Good"),
    UNKNOWN(0.0, "Unknown");

    companion object {
        fun fromValue(value: Double): PM10Level {
            return values().firstOrNull { it.value <= value } ?: UNKNOWN
        }
    }
}

enum class PM2d5Level(val value: Double, val description: String) {
    VERY_POOR(75.0, "Very poor"),
    POOR(50.0, "Poor"),
    MODERATE(25.0, "Moderate"),
    FAIR(10.0, "Fair"),
    GOOD(0.0, "Good"),
    UNKNOWN(0.0, "Unknown");

    companion object {
        fun fromValue(value: Double): PM2d5Level {
            return values().firstOrNull { it.value <= value } ?: UNKNOWN
        }
    }
}

enum class O3Level(val value: Double, val description: String) {
    VERY_POOR(180.0, "Very poor"),
    POOR(140.0, "Poor"),
    MODERATE(100.0, "Moderate"),
    FAIR(60.0, "Fair"),
    GOOD(0.0, "Good"),
    UNKNOWN(0.0, "Unknown");

    companion object {
        fun fromValue(value: Double): O3Level {
            return values().firstOrNull { it.value <= value } ?: UNKNOWN
        }
    }
}

enum class COLevel(val value: Double, val description: String) {
    VERY_POOR(15400.0, "Very poor"),
    POOR(12400.0, "Poor"),
    MODERATE(9400.0, "Moderate"),
    FAIR(4400.0, "Fair"),
    GOOD(0.0, "Good"),
    UNKNOWN(0.0, "Unknown");

    companion object {
        fun fromValue(value: Double): COLevel {
            return values().firstOrNull { it.value <= value } ?: UNKNOWN
        }
    }
}
